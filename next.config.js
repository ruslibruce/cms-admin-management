/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  i18n: {
    locales: ['en-US', 'fr', 'nl-NL', 'kr', 'id-ID'],
    defaultLocale: 'en-US',
  },
  images: {
    formats: ['image/avif', 'image/webp'],
    remotePatterns: [
      {
        protocol: 'https',
        hostname: 'fakestoreapi.com',
        port: '',
        // pathname: '/image/upload/**',
        pathname: '/img/**',
      },
    ],
  },
}

module.exports = nextConfig
