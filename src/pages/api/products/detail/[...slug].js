const DATA_URL = `${process.env.NEXT_PUBLIC_HOST_API}/products`;
// const DATA_URL = "https://jsonplaceholder.typicode.com/todos";

export default async function handler(req, res) {
  const { slug } = req.query;
  if (req.method === "GET") {
    try {
      // Process a GET request
      const result = await fetch(`${DATA_URL}/${slug[0]}`, {
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Headers": "*",
          "Access-Control-Allow-Methods": "*",
          "Access-Control-Allow-Credentials": "true",
        },
      });
      // return res.status(200).json({ result });
      return res.status(200).send(await result.json());
    } catch (error) {
      return res.status(500).send({ error: "failed to fetch data" });
    }
  } else if (req.method === "PUT") {
    // Handle any other HTTP method
    try {
      // Process a PUT request
      const result = await fetch(`${DATA_URL}/${slug[0]}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Headers": "*",
          "Access-Control-Allow-Methods": "*",
          "Access-Control-Allow-Credentials": "true",
        },
        body: req.body,
      });
      // return res.status(200).json({ result });
      return res.status(200).send(await result.json());
    } catch (error) {
      return res.status(500).send({ error: "failed to fetch data" });
    }
  }
}
