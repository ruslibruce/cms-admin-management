import { limiter } from "@/config/limiter";
const DATA_URL = `${process.env.NEXT_PUBLIC_HOST_API}/products`;

export default async function handler(req, res) {
  const remaining = await limiter.removeTokens(1);
  // const origin = req.headers.get('origin');
  if (remaining < 0) {
    return new res(null, {
      status: 429,
      statusText: "Too Many Requests",
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "text/plain",
      },
    });
  }
  if (req.method === "GET") {
    try {
      // Process a POST request
      const result = await fetch(DATA_URL, {
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Headers": "*",
          "Access-Control-Allow-Methods": "*",
          "Access-Control-Allow-Credentials": "true",
        },
      });
      return res.status(200).send(await result.json());
    } catch (error) {
      return res.status(500).send({ error: "failed to fetch data" });
    }
  } else {
    // Handle any other HTTP method
    return res.status(500).send({ error: "failed to fetch data" });
  }
}
