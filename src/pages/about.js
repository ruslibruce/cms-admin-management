import MetaHead from "@/components/MetaHead";

export default function About() {
  return (
    <section>
      <MetaHead
        title={"About"}
        description={"Ini adalah halaman About"}
        url={`${process.env.NEXT_PUBLIC_HOST_NAME}`}
        image={`${process.env.NEXT_PUBLIC_HOST_NAME}/images/logo/nike.svg`}
      />
      <h1 className="text-2xl font-bold">About</h1>
      <p className="py-2">Cupcake icing halvah gummies shortbread halvah brownie dessert cake. Jujubes biscuit cake caramels jelly dragée. Chocolate biscuit biscuit danish croissant candy canes dessert powder. Cake cookie gingerbread tart pastry candy marzipan. Cupcake apple pie cheesecake sweet pie cake brownie pie. Marzipan carrot cake wafer donut cupcake sweet chocolate bar cake marzipan. Danish chocolate cake soufflé chocolate cake lollipop candy canes gummies. Sugar plum fruitcake sesame snaps muffin bonbon shortbread pudding. Candy chocolate bar jelly-o carrot cake jelly beans candy canes. Bear claw sweet dragée chupa chups dessert danish topping topping cake. Donut jelly candy pie tart danish pudding halvah biscuit. Cake tiramisu cupcake topping chupa chups fruitcake croissant cotton candy.</p>
      <p className="py-2">Cake croissant jujubes candy canes chocolate cake bonbon. Cookie cake tootsie roll chocolate bar candy canes biscuit. Lollipop muffin pastry powder danish croissant. Sesame snaps jelly beans jujubes cupcake cupcake. Fruitcake topping gummi bears halvah candy gummies pudding muffin soufflé. Dragée muffin ice cream cookie powder. Halvah pastry lemon drops liquorice chupa chups. Candy canes liquorice powder liquorice toffee sugar plum marzipan dragée bear claw. Topping gingerbread biscuit candy cookie toffee. Apple pie tiramisu lollipop dragée cupcake sesame snaps. Brownie pie donut oat cake dragée danish. Jujubes gingerbread topping macaroon muffin candy canes. Wafer chupa chups liquorice gingerbread brownie cupcake liquorice sugar plum.</p>
    </section>
  )
}