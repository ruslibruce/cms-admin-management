import { NextResponse } from "next/server";

const allowedOrigins =
  process.env.NODE_ENV === "production"
    ? ["https://www.yoursite.com", "https://yoursite.com"]
    : [process.env.NEXT_PUBLIC_HOST_NAME];

export function middleware(request) {
  const origin = request.headers.get("origin");
  console.log(origin);

  if (origin && !allowedOrigins.includes(origin)) {
    return new NextResponse(null, {
      status: 400,
      statusText: "Bad Request",
      headers: {
        'Content-Type': 'text/plain',
      }
    })
  }
  console.log("Middleware!!");
  console.log(request.method);
  console.log(request.url);
  return NextResponse.next();
  // return NextResponse.redirect(new URL('/home', request.url));
}

export const config = {
  matcher: "/api/:path*",
};
