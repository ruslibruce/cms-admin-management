import Footer from "./Footer";
import Header from "./Header";

export default function Layout({ children }) {
    return (
        <>
            <Header />
            <main className="max-w-screen-xl mt-20 mx-auto p-4">
                {children}
            </main>
            <Footer />
        </>
    )
}